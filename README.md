# Create image style
```php
$imageStyle = \Drupal\image\Entity\ImageStyle::create(['label' => 'XXX'])->save();
```

# Create effect
```php
$configuration = array(
  'uuid' => NULL,
  'id' => 'image_scale_and_crop',
  'weight' => 0,
  'data' => array(
    'width' => XXX,
    'height' => XXX,
  ),
);
$effect = \Drupal::service('plugin.manager.image.effect')->createInstance($configuration['id'], $configuration);
```

# Add it to the image style and save
```php
$imageStyle->addImageEffect($effect->getConfiguration());
$imageStyle->save();
```

# Generate URL of an image with a style applied
```php
$file = \Drupal\file\Entity\File::load(XXX);
$imageStyle = \Drupal\image\Entity\ImageStyle::load('XXX')->buildUrl($file->getFileUri());
```

# Load all image styles
```php
$imageStyles = \Drupal\image\Entity\ImageStyle::loadMultiple();
```

# Render an image with style
```php
$file = \Drupal\file\Entity\File::load(XXX);
$paramsRender = array(
    '#theme' => 'image_style',
    '#style_name' => 'XXX',
    '#uri' => $file->getFileUri(),
);
$output = \Drupal::service('renderer')->render($paramsRender);
```